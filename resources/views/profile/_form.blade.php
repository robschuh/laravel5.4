<!-- forename Form Input -->
<div class="form-group @if ($errors->has('forename')) has-error @endif">
    {!! Form::label('forename', 'Forename') !!}
    {!! Form::text('forename', null, ['class' => 'form-control', 'placeholder' => 'Forename']) !!}
    @if ($errors->has('forename')) <p class="help-block">{{ $errors->first('forename') }}</p> @endif
</div>

<!-- surname Form Input -->
<div class="form-group @if ($errors->has('surname')) has-error @endif">
    {!! Form::label('surname', 'Surname') !!}
    {!! Form::text('surname', null, ['class' => 'form-control', 'placeholder' => 'Surname']) !!}
    @if ($errors->has('surname')) <p class="help-block">{{ $errors->first('surname') }}</p> @endif
</div>

<!-- Text bio Form Input -->
<div class="form-group @if ($errors->has('bio')) has-error @endif">
    {!! Form::label('bio', 'Bio') !!}
    {!! Form::textarea('bio', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Body of Post...']) !!}
    @if ($errors->has('bio')) <p class="help-block">{{ $errors->first('bio') }}</p> @endif
</div>

<div class="form-group @if ($errors->has('user_photo')) has-error @endif">
    {{Form::label('user_photo', 'User Photo',['class' => 'control-label'])}}
    {{Form::file('user_photo')}}
    @if($profile->user_photo)
     <img src="/storage/user_photo/{{ $profile->user_photo }}" id="userPhoto" />
     <i class="glyphicon glyphicon-trash" id="file"></i>
    @endif
</div>
