@extends('layouts.app')

@section('title', 'Edit Profile ' . $profile->forename)

@section('content')
      
    <div class="row">
        <div class="col-md-5">
            <h3>Edit {{ $profile->forename }} profile</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('profiles.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        {!! Form::model($profile, ['method' => 'PUT', 'route' => ['profiles.update',  $profile->id ], 'files' => true ]) !!}
                            @include('profile._form')
                            <!-- Submit Form Button -->
                            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

        <script type="text/javascript">
        $( document ).ready(function() {

        $( "#file" ).click(function() {
            console.log("file click");
        $.get( "/profile/photo/<?php echo $profile->id; ?>")
          .done(function( data ) {
            console.log( "Photo removed: " + data );
            // Remove image
            $( "#userPhoto" ).remove();
            
          });
        });  
   }); 
    </script>

@endpush

 