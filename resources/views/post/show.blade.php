@extends('layouts.app')

@section('title', 'Posts')

@section('content')
    <div class="row">
        <div class="col-md-5">
            <h3 class="modal-title">{{ $post->title }}</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            @can('add_posts')
                <a href="{{ route('posts.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
            @endcan
        </div>
    </div>

    <div class="result-set">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Author</th>
                <th>Created At</th>
                @can('edit_posts', 'delete_posts')
                    <th class="text-center">Actions</th>
                @endcan
            </tr>
            </thead>
            <tbody>
         
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->user['name'] }}</td>
                    <td>{{ $post->created_at->toFormattedDateString() }}</td>
                    @can('edit_posts', 'delete_posts')
                    <td class="text-center">
                        @include('shared._actions', [
                            'entity' => 'posts',
                            'id' => $post->id
                        ])
                    </td>
                    @endcan
                </tr>
           
            </tbody>
        </table>

    </div>

@endsection