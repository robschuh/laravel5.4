@extends('layouts.app')

@section('title', 'Users')

@section('content')
    <div class="row">
        <div class="col-md-5">
            <h3 class="modal-title">{{ $result->total() }} {{ str_plural('User', $result->count()) }} </h3>
        </div>
        <div class="col-md-7 page-action text-right">
            @can('add_users')
                <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
            @endcan
        </div>
    </div>

    <div class="result-set">
       
            <tbody>
            <div class="container-fluid">
                 <div class="row">
                    @foreach($result as $item)
                        <div class="col-md-3 pull-left">
                            <p>{{ $item->id }}</p>
                            
                            <p>
                            @if (isset($usersPhotos[$item->id]) && $usersPhotos[$item->id] != null)                                                 
                                <?php $urlPhoto = asset('storage/user_photo/' . $usersPhotos[$item->id]); ?>                             
                            @else
                                <?php $urlPhoto = asset('storage/user_photo/demo.png'); ?>
                            @endif                                               
                            <img src="<?php echo $urlPhoto; ?>" />
                            </p>
                            
                            <p  style="display: inline">       
                            @include('shared._actions', [
                                    'entity' => 'users',
                                    'id' => $item->id
                                ]) 
                            </p>
                            
                            <p>
                                {{ $item->name }} 
                            </p>
                            
                            <p>{{ $item->email }}</p>
                            
                            <p>{{ $item->roles->implode('name', ', ') }}</p>
                            
                            <p>{{ $item->created_at->toFormattedDateString() }}</p>


                        </div>

                    @endforeach
                 </div>
             </div>    
            </tbody>
       

        <div class="text-center">
            {{ $result->links() }}
        </div>
    </div>

@endsection