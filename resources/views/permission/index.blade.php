@extends('layouts.app')

@section('title', 'Permissions')

@section('content')
    <div class="row">
        <div class="col-md-5">
            <h3 class="modal-title">Permission</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            @can('add_posts')
                <a href="{{ route('permissions.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
            @endcan
        </div>
    </div>

    <div class="result-set">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
        
                <th>Created At</th>
                @can('edit_posts', 'delete_posts')
                    <th class="text-center">Actions</th>
                @endcan
            </tr>
            </thead>
            <tbody>
            @foreach($permissions as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                   
                    <td>{{ $item->created_at->toFormattedDateString() }}</td>
                    @can('edit_posts', 'delete_posts')
                    <td class="text-center">
                        @include('shared._actions', [
                            'entity' => 'permissions',
                            'id' => $item->id
                        ])
                    </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-center">
            {{ $permissions->links() }}
        </div>
    </div>

@endsection