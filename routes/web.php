<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
 
//Route::get('/', 'HomeController@index')->name('home');

Route::group( ['middleware' => ['auth']], function() {
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('posts', 'PostController');
    Route::resource('comments', 'CommentController');
  
    Route::resource('profiles', 'ProfileController');
    Route::get('/profile/photo/{user}', 'ProfileController@destroy_photo')->name('destroy.profilePhoto');
    Route::get('/user/perms/{user}', 'UserController@getUserPermissions')->name('users.permissions');

});

// Administration
Route::group( ['middleware' => ['auth', 'role:admin']], function() {
  Route::resource('roles', 'RoleController');
  Route::resource('permissions', 'PermissionController');
});

Route::get('/', function () {
    return view("bootstrap");
});



Route::get('/api/users', 'UserController@index')->name('users.index.api');

Route::get("/api/animals", function() {
    return response()->json([['name' => 'dog'], ['name' => 'cat'], ['name' => 'elephant'], ['name' => 'elk'], ['name' => 'spider']]);
});