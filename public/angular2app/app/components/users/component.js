System.register(["angular2/core", "../../services/api"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, api_1;
    var Users;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (api_1_1) {
                api_1 = api_1_1;
            }],
        execute: function() {
            Users = (function () {
                function Users(api) {
                    var _this = this;
                    this.api = api;
                    this.api.getUsers().then(function (res) {
                        console.log(res[0].data);
                        _this.users = res[0].data;
                    }, function (error) {
                        console.log("Error: " + JSON.stringify(error));
                    });
                }
                Users = __decorate([
                    core_1.Component({
                        selector: "users",
                        template: "\n    <div class=\"container-fluid\">  \n        <div class=\"row\">\n            <div class=\"col-md-3 pull-left\"  *ngFor=\"#user of users\">      \n                <p>{{ user.name }}</p>\n                <p>{{ user.email }}</p>\n                <p>{{ user.profile?.forename }}</p>\n               \n                <p *ngIf=\"user.profile?.user_photo\"><img src=\"/storage/user_photo/{{ user.profile.user_photo }}\" /></p>          \n            </div>\n        </div> \n    </div>\n    ",
                        providers: [api_1.Api]
                    }), 
                    __metadata('design:paramtypes', [api_1.Api])
                ], Users);
                return Users;
            }());
            exports_1("Users", Users);
        }
    }
});
//# sourceMappingURL=component.js.map