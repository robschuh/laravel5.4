import {Component} from "angular2/core"
import {Api} from "../../services/api"

@Component({
    selector: "users",
    template: `
    <div class="container-fluid">  
        <div class="row">
            <div class="col-md-3 pull-left"  *ngFor="#user of users">      
                <p>{{ user.name }}</p>
                <p>{{ user.email }}</p>
                <p>{{ user.profile?.forename }}</p>
               
                <p *ngIf="user.profile?.user_photo"><img src="/storage/user_photo/{{ user.profile.user_photo }}" /></p>          
            </div>
        </div> 
    </div>
    `,
    providers: [Api]
})

export class Users { 
    users: Array<any>;
    constructor(private api: Api){
        this.api.getUsers().then(
            (res) => {
                console.log(res[0].data);
                this.users = res[0].data;
            },
            (error) => {
                console.log("Error: " + JSON.stringify(error));
            }
        )
   
    }
}