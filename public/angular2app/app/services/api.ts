import {Injectable} from "angular2/core";
import {Http} from "angular2/http";

@Injectable()
export class Api {

    apiUrl: string = "http://lara5.4.dev/api/";
    constructor(private http: Http) {

    }

    getAnimals() {
        return new Promise((resolve, reject) => {
            this.http.get(this.apiUrl + "animals").subscribe(
                res => {
                    resolve(res.json());
                },
                error => {
                    reject(error);
                }
            )
        })
    }
    
    
    getUsers() {
        return new Promise((resolve, reject) => {
            this.http.get(this.apiUrl + "users").subscribe(
                res => {
                    resolve(res.json());
                },
                error => {
                    reject(error);
                }
            )
        })
    }
}