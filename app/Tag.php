<?php

namespace App;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasRoles;
    protected $guard_name = 'web'; 
     
    public function posts() {
        return $this->hasMany(Post::class);
    }
}
