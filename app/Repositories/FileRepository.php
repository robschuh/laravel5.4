<?php

namespace App\Repositories;
use Intervention\Image\Facades\Image;

class FileRepository {

	public function delete($path){

            // Delete previous photo if exists
	    $deleted = false;

	    //$deleted = Storage::delete($path);
	    if(file_exists($path)) {
	        $deleted = unlink($path);
	    } 
   
	    return $deleted;
	}

	public function upload($request, $path){
		//var_dump($path);  
        $file = $request->file('user_photo');
        $photoName = time() . '.' . $file->getClientOriginalExtension();

        $file->move($path, $photoName);
        
        $image = Image::make($path . $photoName);
        $image->resize(98, 98);
        $image->save($path  . $photoName);

        return $image;
	}
}