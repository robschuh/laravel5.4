<?php

namespace App;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasRoles;
    
    protected $guard_name = 'web'; 
}
