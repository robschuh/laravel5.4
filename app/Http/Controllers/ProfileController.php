<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Repositories\FileRepository;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {

        $profile = DB::table('profiles')->where('user_id', '=', $user_id)->first();

        // If user has not any profile yet, create one.
        if(!$profile){
            if ($profile = Profile::firstOrCreate(['user_id' => $user_id])) {
                $profile->save();            
                flash('Profile has been created, please fill these data.');
                return redirect()->route('profiles.edit', ['user_id' => $user_id]);

            } 
        }

        return view('profile.edit', compact('profile'));

      //  return redirect()->route('users.edit', ['user_id' => $user_id]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile, FileRepository $fileRepository)
    {
        $this->validate($request, [
          'forename' => 'required|min:3',
          'surname' => 'required|min:3',
          'bio' => 'required|min:15',
        ]);

       
        $profile = Profile::findOrFail($profile->id);
      
        $photoName = 'demo.png';

        if ($request->hasFile('user_photo')) { 
            if ($request->file('user_photo')->isValid()) {
                try {
                   
                    $path = public_path() . '/storage/user_photo/'; 
                    
                    // Delete a previous file if exists.
                    if(!is_null($profile->user_photo) && $profile->user_photo != 'demo.png'){                  
                        $fileRepository->delete($path . $profile->user_photo);
                    }

                    $image = $fileRepository->upload($request, $path);
                    $profile->user_photo = $image->basename;

                } catch (Illuminate\Filesystem\FileNotFoundException $e) {
                    
                }
            }
            
        }

        $profile->update($request->all());
        flash()->success('Profile has been updated.');
        
        $avoidCache = rand(0, 10000);
        return redirect('profiles/' .$profile->user['id']. '/edit?cache=' .$avoidCache);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile) {}

    /**
     * Ajax callback to delete a photo.
     * @param User $user
     * @return type
     * 
     */
    public function destroy_photo(User $user, FileRepository $fileRepository){

        $deleted = false;
        
        // Deleting thumb image
        $path = public_path() . '/storage/user_photo/' . $user->profile['user_photo'];
        //$deleted = Storage::delete($path);
        
        if ($fileRepository->delete($path)) { 
            $profile = Profile::find($user->profile['id']);
            $profile['user_photo'] = 'demo.png';
            if (!$profile->save()){
                return response('The image profile was deleted!, but there is an issue updating the db', 201)->header('Content-Type', 'text/plain');
            }

            return response('The image profile was deleted successfully!', 200)
            ->header('Content-Type', 'text/plain');
        }
        return response('The image profile ' .  $path . ' was not deleted!', 201)
        ->header('Content-Type', 'text/plain');
    }
}

