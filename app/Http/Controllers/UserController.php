<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Permission;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller {

    public function index(Request $request) {
        $result = User::latest()->with('profile')->paginate();
        $usersPhotos = array();
        foreach ($result as $item) {

            if ($item->profile['user_photo'] != null) {
                // configure with favored image driver (gd by default)
                Image::configure(array('driver' => 'imagick'));

                // and you are ready to go ...
                $usersPhotos[$item->id] = $item->profile['user_photo'];
            }
        }
        
        // Api queries.
        if($request->is('api/*')){
            return response()->json([$result, 'status' => 200]);
        }


        return view('user.index', compact('result', 'usersPhotos'));
    }

    public function create() {
        $roles = Role::pluck('name', 'id');
        return view('user.new', compact('roles'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'roles' => 'required|min:1'
        ]);
        

        // hash password
        $request->merge(['password' => bcrypt($request->get('password'))]);

        // Create the user
        if ($user = User::create($request->except('roles', 'permissions'))) {
            
            // Assign role by default.
            $user->assignRole('user');
            $user->save();
          //  $this->syncPermissions($request, $user);
            flash('User has been created.');
        } else {
            flash()->error('Unable to create user.');
        }

        return redirect()->route('users.index');
    }

    public function edit($user_id) {
        
        $user = Auth::user();
 
        // Only users with this permission can edit no owned user profiles.
        if(!$user->hasPermissionTo('edit_all users')){
            if($user->id != $user_id){
                flash()->success('You have not permission papi!.');
                return redirect('profiles/' . $user->id . '/edit');
            }
        }
         
       
        $roles = Role::pluck('name', 'id');
        $permissions = Permission::all('name', 'id');

        return view('user.edit', compact('user', 'roles', 'permissions'));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users,email,' . $id,
            'roles' => 'required|min:1'
        ]);

        // Get the user
        $user = User::findOrFail($id);

        // Update user
        $user->fill($request->except('roles', 'permissions', 'password'))->save();

        // check for password change
        if ($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        $roles = $request['roles'];
        if (isset($roles)) {        
            $user->roles()->sync($roles);          
        }        
        else {
            $user->roles()->detach();
        }
       
      
        flash()->success('User has been updated.');
        return redirect('profiles/' . $user->id . '/edit');
    }

    public function destroy($id) {
        if (Auth::user()->id == $id) {
            flash()->warning('Deletion of currently logged in user is not allowed :(')->important();
            return redirect()->back();
        }

        if (User::findOrFail($id)->delete()) {
            flash()->success('User has been deleted');
        } else {
            flash()->success('User not deleted');
        }

        return redirect()->back();
    }

    private function syncPermissions(Request $request, $user) {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if (!$user->hasAllRoles($roles)) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);
        return $user;
    }
    
    public function getUserPermissions(User $user){
       
        // All permissions which apply on the user (inherited and direct)
        $perms = $user->getAllPermissions();
        foreach ($perms as $perm){

            var_dump($perm->name);
        }
        
        $users = User::role('admin')->get(); 
        
        foreach ($users as $user){
            var_dump($user);
        }
    }

}
