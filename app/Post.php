<?php

namespace App;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    use HasRoles;

    protected $fillable = ['title', 'body'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function tag() {
        return $this->hasMany(Tag::class);
    }
}
