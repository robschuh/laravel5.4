<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;

class Profile extends Model
{

    use HasRoles;
    protected $guard_name = 'web'; 
    
    protected $fillable = ['forename', 'surname', 'bio', 'created_at', 'updated_at', 'user_id'];

    
    public function user() {
        return $this->belongsTo(User::class);
    }
}
